class ApplicationController < ActionController::Base
  protect_from_forgery
  helper_method :current_user
  helper_method :options_for_place
  helper_method :user_language
  # after_filter :set_access_control_headers
  before_filter :set_user_language
  before_filter :check_authentication

  PAGE = 5
  PER_PAGE = 5

  def after_login_path
    reports_path
  end

  def check_authentication
    unless current_user
      return redirect_to login_path
    end
  end


  def user_language
    session[:lan] || 'en'
  end

  private
  def set_user_language
    I18n.locale = params[:locale]
    session[:lan] = I18n.locale
  end

  # app/controllers/application_controller.rb
  def default_url_options(options={})
    logger.debug "default_url_options is passed options: #{options.inspect}\n"
    { locale: I18n.locale }
  end


  def generate_csv_headers(filename)
    headers.merge!({
      'Cache-Control'             => 'must-revalidate, post-check=0, pre-check=0',
      'Content-Type'              => 'text/csv',
      'Content-Disposition'       => "attachment; filename=\"#{filename}\"",
      'Content-Transfer-Encoding' => 'binary'
    })
  end
  
  private
  
  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  # def set_access_control_headers 
  #   headers['Access-Control-Allow-Origin'] = 'http://localhost:3000/' 
  #   headers['Access-Control-Request-Method'] = '*' 
  # end

  def options_for_place user
    if user.level == 1 
        user_places = Phd.all.to_a
    else
        user_places = user.place.children.to_a      
    end
    
    result_opt = []
    User.construct_place user_places, result_opt, 0
    result_opt
  end

  def enable_access_level(place)
    flag = true
    if current_user.level == 2
      if place.id == current_user.place_id  || place.parent_id == current_user.place_id || current_user.place.children.find(place.parent_id)
        flag =  true
      else
        flag = false
      end
    end    
    flag
  end

end
