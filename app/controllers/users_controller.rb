class UsersController < ApplicationController

  # helper_method :options_for_place

    
  def index
    if params[:name].present?
      @users = User.where("lower(name) like ?", "%#{params[:name].downcase}%")
    else
      @users = current_user.members      
    end

    if !@users.nil?
      @per_page = 10
      @page = params[:page] || 1
      @users = @users.paginate(:page => params[:page], :per_page => @per_page)
    end
    
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new params[:user]
    if @user.save
      # flash['success'] = "User is created successfully"
      redirect_to users_url, notice: t('user.message.create_success')
    else
      render action: "new"
    end
  end

  def edit
    @user_param = User.find(params[:id])
    if current_user.is_member(@user_param)
      @user = @user_param
    else
      redirect_to users_url, alert: t('user.message.no_edit_authorize')
    end
    
    # @parent_user = User.find(@user.parent_id)
  end

  def update
    @user_param = User.find(params[:id])
    if current_user.is_member(@user_param) 
      @user = @user_param      
      if @user.update_attributes(params[:user])
        redirect_to users_url, notice: t('user.message.update_success')
      else
        render action: "edit"
      end
    else
      redirect_to users_url, alert: t('user.message.no_edit_authorize')
    end
  end

  def destroy
    @user = User.find(params[:id])
    if current_user.is_member(@user)
      @user.destroy      
      redirect_to users_url, notice: t('user.message.delete_success')      
    else
      redirect_to users_url, alert: t('user.message.no_edit_authorize')
    end
    
  end

  def reset_password
    @user = current_user
  end

  def change_password
    @user = current_user
    if @user.change_password(params[:user])
      # flash["success"] = "Updated successfully"
      redirect_to reports_url, notice: t('user.message.password_updated')
    else
      render action: "reset_password"
    end
  end

  # def update_phone
  #   @user = User.where(:phone_number => params[:phone_number])
  #   response = @user.update_all(:phone_number => nil)
  #   if response
  #     render json: response
  #   else
  #     render json: nil
  #   end
  # end

  def update_current_user
    @user = User.find(current_user.id)
    if @user.update_attributes(params[:user])
      redirect_to users_url, notice: t('user.message.update_success')
    else
      render action: "view"
    end
  end
  # def update_current_user
  #   @user_param = User.find(params[:user_id])
  #   if current_user.is_member(@user_param) 
  #     @user = @user_param      
  #     if @user.update_attributes(params[:user])
  #       redirect_to users_url, notice: 'User was successfully updated.'
  #     else
  #       render action: "view"
  #     end
  #   else
  #     redirect_to users_url, alert: 'You are not authorize to edit this user.'
  #   end
  # end

  def view
    @user = User.find(params[:user_id])
    # @parent_user = User.find(@user.parent_id)
  end

  # def options_for_place
  #   if current_user.name == 'admin'
  #     user_places = Phd.all.to_a
  #   else
  #     user_places = current_user.place.children.to_a
  #   end
  #   result_opt = []
  #   User.construct_place user_places, result_opt, 0
  #   result_opt
  # end

end