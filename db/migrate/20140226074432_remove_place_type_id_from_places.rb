class RemovePlaceTypeIdFromPlaces < ActiveRecord::Migration
  def up
    remove_column :places, :place_type_id
      end

  def down
    add_column :places, :place_type_id, :string
  end
end
